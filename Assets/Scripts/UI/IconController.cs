using GeneratedInteractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

class IconController
{
    Dictionary<string, string> PicturesUnloaded;
    Dictionary<string, string> PicturesAny;
    Dictionary<string, Sprite> Pictures;

    public IconController()
    {
        PicturesUnloaded = new Dictionary<string, string>();
        Pictures = new Dictionary<string, Sprite>();

        // searches thru every picture file in the directory, logs them to a dictionary PicturesUnloaded
        foreach (string filepath in GetPictureFiles())
        {
            string filename = Path.GetFileNameWithoutExtension(filepath).ToLower();

            // disregard any picture that isnt a base image or icon image
            if (filename.ToLower().Contains("-"))
            {
                string flags = filename.Split('-')[1];
                if (flags != "" && flags != "icon" && flags != "fem" && flags != "masc")
                    continue;
            }

            PicturesUnloaded[filename] = filepath;
        }

        PicturesAny = new Dictionary<string, string>(PicturesUnloaded);
    }

    /// <summary>Return all valid picture files.</summary>
    internal static List<string> GetPictureFiles()
    {
        var files = Directory
            .GetFiles(
                Path.Combine(Application.streamingAssetsPath, "Pictures"),
                "*.*",
                SearchOption.AllDirectories
            )
            .ToList();
        files.AddRange(
            Directory.GetFiles(State.PicDirectory, "*.*", SearchOption.AllDirectories)
        );

        return files
            .Where(
                s =>
                    (s.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase)
                    || s.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase)
                    || s.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase))
            )
            .ToList();
    }

    internal Sprite AttemptLoad(string name, bool warnMissing = true)
    {
        // if Pictures dict already has an image assigned to this gilename, use that
        if (Pictures.ContainsKey(name))
            return Pictures[name];

        // if PicturesUnloaded has an image file assigned to this name, load it into the Pictures dict and use that
        // Also removes the picture from "PicturesUnloaded" (as it is now loaded)
        if (PicturesUnloaded.ContainsKey(name))
        {
            Pictures[name] = LoadPNG(PicturesUnloaded[name]);
            PicturesUnloaded.Remove(name);
            return Pictures[name];
        }

        // else, return null & throw a warning if warnMissing = true
        else
        {
            if (warnMissing)
                Debug.LogWarning($"No Icon for: \"{name}\"");
            return null;
        }
    }

    internal Sprite GetIcon(Person person)
    {
        Sprite icon;

        // check for a special icon image, if one exists
        icon = AttemptLoad(person.Picture.ToLower() + "-icon", warnMissing: false);
        if (icon)
            return icon;

        // check for a base image
        icon = AttemptLoad(person.Picture.ToLower(), warnMissing: false);
        if (icon)
            return icon;

        // check for a base image with a single dash after (VState Gen exports filenames like this)
        icon = AttemptLoad(person.Picture.ToLower() + "-", warnMissing: false);
        if (icon)
            return icon;

        // use default images
        if (Config.DisableDefaultImages == false)
        {
            if (person.GenderType.HasBreasts)
            {
                icon = AttemptLoad("default-fem");
                if (icon)
                    return icon;
            }
            else
            {
                icon = AttemptLoad("default-masc");
                if (icon)
                    return icon;
            }
        }

        // check for a default icon image
        icon = AttemptLoad("default-icon", warnMissing: false);
        if (icon)
            return icon;

        // finally, check for a default image or otherwise load null
        return AttemptLoad("default", warnMissing: false);
    }

    static Sprite LoadPNG(string filePath)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2, TextureFormat.BGRA32, false);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        if (tex == null)
            return null;

        Rect rect = new Rect(new Vector2(0, 0), new Vector2(tex.width, tex.height));
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        int higherDimension = Math.Max(tex.width, tex.height);

        // Crop icons to square size
        {
            var texRatio = ((float)tex.height / (float)tex.width);
            var targetRatio = 1;

            if (texRatio > targetRatio) // adjust an image that is too tall
            {
                var heightAdj = (int)(tex.width * targetRatio);
                rect.Set(0, (tex.height - heightAdj), tex.width, heightAdj);
                higherDimension = Math.Max(tex.width, heightAdj);
            }
        }
                
        Sprite sprite = Sprite.Create(tex, rect, pivot, higherDimension);
        return sprite;
    }
}
