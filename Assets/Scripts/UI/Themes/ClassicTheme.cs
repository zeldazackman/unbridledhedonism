using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ClassicTheme : MonoBehaviour
{

    public Color themeColor;
    public Color HighlightColor;
    public Color PressedColor;
    public Color LabelColor;
    public Color TextColor;

    public void ChangeTheme()
    {
        Debug.Log("hello");
        // loop through all the UI elements in the scene
        foreach (var uiElement in FindObjectsOfType<Graphic>())
        {
            // check if the element is a Button
            if (uiElement.GetComponent<Button>())
            {
                // change the normal color of the Button to the new theme color
                var colors = uiElement.GetComponent<Button>().colors;
                colors.normalColor = themeColor;
                uiElement.GetComponent<Button>().colors = colors;
            }
            if (uiElement.GetComponent<TextMeshProUGUI>())
            {
                uiElement.color = TextColor;
            }
            if (uiElement.name == "Panel")
            {
                uiElement.color = LabelColor;
            }
        }
    }
}
