﻿namespace Assets.Scripts.UI.Connectors
{
    interface ITooltip
    {
        string Text { get; set; }
    }
}
