﻿using Assets.Scripts.UI.Connectors;
using UnityEngine;
using UnityEngine.EventSystems;

public class StartScreenTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ITooltip
{
    [SerializeField]
    private string text;
    public string Text
    {
        get => text;
        set => text = value;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (State.GameManager.StartScreen.gameObject.activeSelf)
            State.GameManager.StartScreen.ChangeToolTip(Text);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (State.GameManager.StartScreen.gameObject.activeSelf)
            State.GameManager.StartScreen.ChangeToolTip("");
    }
}
