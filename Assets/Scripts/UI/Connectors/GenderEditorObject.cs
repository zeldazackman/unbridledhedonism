﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GenderEditorObject : MonoBehaviour
{
    [Description("The name for this type")]
    public TMP_InputField Name;

    [Description("Does this type have breasts?")]
    public Toggle HasBreasts;

    [Description(
        "Does this type have a vagina? (It's strongly recommended a type has at least one of the two main sex organs)"
    )]
    public Toggle HasVagina;

    [Description(
        "Does this type have a dick? (It's strongly recommended a type has at least one of the two main sex organs)"
    )]
    public Toggle HasDick;

    [Description("Does this type have a feminine appearance?")]
    public Toggle Feminine;

    [Description("Does this type have a use a feminine name?")]
    public Toggle FeminineName;

    [Description("The pronoun set used by this type")]
    public TMP_Dropdown PronounSet;

    [Description("Do characters with a 'Male Only' orientation like this type?")]
    public Toggle Male;

    [Description("Do characters with a 'Female Only' orientation like this type?")]
    public Toggle Female;

    [Description(
        "Do characters with a 'Androsexual' (attracted to masculinity) orientation like this type?"
    )]
    public Toggle Androsexual;

    [Description(
        "Do characters with a 'Gynesexual' (attracted to femininity) orientation like this type?"
    )]
    public Toggle Gynesexual;

    [Description("Do characters with an 'Exclusively Bisexual' orientation like this type?")]
    public Toggle ExclusiveBi;

    [Description("Do characters with a 'Skoliosexual' orientation like this type?")]
    public Toggle Skoliosexual;

    [Description(
        "How often this type gets picked.  If you have say, males at at half, and females at max, then 2/3rds of characters will be female"
    )]
    public Slider RandomWeight;

    [Description("Do characters with a 'Skoliosexual' orientation like this type?")]
    public Toggle VoreDisabled;

    [Description(
        "Sets the color of this gender if the gender colors are on in the options menu.  Stored in Hex, in rgb format. (Search 'Hex color' on the internet for more details) "
    )]
    public TMP_InputField Color;

    private void Start()
    {
        WireUpTooltips.WireUp<GenderEditorObject, GenderScreenTooltip>(this);
    }
}
