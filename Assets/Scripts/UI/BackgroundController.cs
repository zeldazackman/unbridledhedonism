using GeneratedInteractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

class BackgroundController
{
    HashSet<string> PicturesUnloaded;
    HashSet<string> PicturesAny;
    Dictionary<string, Sprite> Pictures;

    public BackgroundController()
    {
        PicturesUnloaded = new HashSet<string>();
        Pictures = new Dictionary<string, Sprite>();

        foreach (string file in GetPictureFiles())
            PicturesUnloaded.Add(Path.GetFileNameWithoutExtension(file).ToLower());

        PicturesAny = new HashSet<string>(PicturesUnloaded);

        InitializePictures();
    }

    /// <summary>Return all valid picture files.</summary>
    internal static List<string> GetPictureFiles()
    {
        var files = Directory
            .GetFiles(
                Path.Combine(Application.streamingAssetsPath, "Backgrounds"),
                "*.*",
                SearchOption.AllDirectories
            )
            .ToList();
        files.AddRange(
            Directory.GetFiles(State.BackgroundDirectory, "*.*", SearchOption.AllDirectories)
        );
        return files
            .Where(
                s =>
                    s.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase)
                    || s.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase)
                    || s.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase)
            )
            .ToList();
    }

    internal void InitializePictures()
    {
        foreach (string file in GetPictureFiles())
        {
            Pictures[Path.GetFileNameWithoutExtension(file).ToLower()] = LoadPNG(file);
        }
    }

    internal Sprite AttemptLoad(string name, bool warnMissing = true)
    {
        if (Pictures.ContainsKey(name))
            return Pictures[name];
        else
        {
            if (warnMissing)
                Debug.LogWarning($"No background for: \"{name}\"");
            return null;
        }
    }

    internal Sprite GetBackground(Person person)
    {
        Sprite background = null;

        // if eaten, display an internal background if one exists
        if (person.BeingEaten)
        {
            string vorelocation = person.VoreTracking.LastOrDefault().Location.ToString().ToLower();

            // check for a background whose filename matches a vore location name with extension for swallowing
            if (person.BeingSwallowed)
            background = AttemptLoad(vorelocation+"swallow", warnMissing: false);
            if (background)
                return background;

            // check for a background whose filename matches the vore location name (ie: "balls", "womb", etc)
            background = AttemptLoad(vorelocation, warnMissing: false);
            if (background)
                return background;
            
            // if a vore location specific image does not exist, use a generic "eaten" image
            background = AttemptLoad("eaten");
            if (background)
                return background;
        }

        // if there is a background whose filename matches the current room's name, display it
        string locName = State.World.Zones[person.Position.x, person.Position.y].Name.ToLower();
        {
            background = AttemptLoad(locName, warnMissing: false);
            if (background)
                return background;
        }

        // else, check the room's generic name
        string locType = State.World.Zones[person.Position.x, person.Position.y].Type.ToString().ToLower();
        {
            background = AttemptLoad(locType);
            if (background)
                return background;
        }

        // finally, check for a default background or otherwise load null
        return AttemptLoad("default", warnMissing: false);
    }

    static Sprite LoadPNG(string filePath)
    {
        if (!File.Exists(filePath))
            return null;

        byte[] fileData = File.ReadAllBytes(filePath);
        Texture2D tex = new Texture2D(2, 2, TextureFormat.BGRA32, false);
        if (!tex.LoadImage(fileData)) //..this will auto-resize the texture dimensions.
        {
            Debug.LogError($"Failed to load background: {filePath}");
            return null;
        }
        if (tex == null)
            return null;

        Rect rect = new Rect(new Vector2(0, 0), new Vector2(tex.width, tex.height));
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        int higherDimension = Math.Max(tex.width, tex.height);

        Sprite sprite = Sprite.Create(tex, rect, pivot, higherDimension);
        return sprite;
    }
}
