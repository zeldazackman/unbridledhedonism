﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoveredText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    bool hovering;

    public bool RespectsBlockedPopups;

    public TextMeshProUGUI InfoText;

    private void Update()
    {
        if (hovering == false)
            return;

        int wordIndex = TMP_TextUtilities.FindIntersectingWord(InfoText, Input.mousePosition, null);
        int nameIndex = TMP_TextUtilities.FindIntersectingLink(InfoText, Input.mousePosition, null);

        if (nameIndex > -1)
        {
            var id = InfoText.textInfo.linkInfo[nameIndex].GetLinkID();
            if (Input.GetMouseButtonDown(0))
            {
                State.GameManager.HoveringTooltip.Clicked(id);
            }
            else if (Input.GetMouseButtonDown(1))
            {
                State.GameManager.HoveringTooltip.RightClicked(id);
            }
            else
                State.GameManager.HoveringTooltip.UpdateInformation(id, RespectsBlockedPopups);
            return;
        }

        if (wordIndex > -1)
        {
            string[] words = new string[5];
            for (int i = 0; i < 5; i++)
            {
                if (
                    wordIndex - 2 + i < 0
                    || wordIndex - 2 + i >= InfoText.textInfo.wordCount
                    || InfoText.textInfo.wordInfo[wordIndex - 2 + i].characterCount < 1
                )
                {
                    words[i] = string.Empty;
                    continue;
                }
                words[i] = InfoText.textInfo.wordInfo[wordIndex - 2 + i].GetWord();
            }

            State.GameManager.HoveringTooltip.UpdateInformation(words, RespectsBlockedPopups);
            if (Input.GetMouseButtonDown(0))
            {
                State.GameManager.HoveringTooltip.Clicked(words);
            }
            if (Input.GetMouseButtonDown(1))
            {
                State.GameManager.HoveringTooltip.RightClicked(words);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        hovering = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hovering = false;
    }
}
