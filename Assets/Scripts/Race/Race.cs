﻿using System.Collections.Generic;

public class Race
{
    internal string ParentRaceString;
    internal Race ParentRace;
    internal string Name;
    internal bool PickableRace;
    internal bool NamedCharacter;

    internal string[] HairColorMale;
    internal string[] HairColorFemale;
    internal string[] EyeColorFemale;
    internal string[] EyeColorMale;
    internal string[] ShoulderDescriptionFemale;
    internal string[] ShoulderDescriptionMale;
    internal string[] HipDescriptionFemale;
    internal string[] HipDescriptionMale;
    internal string[] HairLengthFemale;
    internal string[] HairLengthMale;
    internal string[] HairStyleFemale;
    internal string[] HairStyleMale;
    internal string[] QuirksFemale;
    internal string[] QuirksMale;
    internal string[] TraitsFemale;
    internal string[] TraitsMale;

    internal float[] HeightRangeFemale;
    internal float[] HeightRangeMale;
    internal float[] WeightRangeFemale;
    internal float[] WeightRangeMale;

    internal float[] BreastSizeFemale;
    internal float[] BreastSizeMale;
    internal float[] DickSizeFemale;
    internal float[] DickSizeMale;
    internal float[] BallSizeFemale;
    internal float[] BallSizeMale;

    internal Dictionary<string, string[]> FeminineTag = new Dictionary<string, string[]>();
    internal Dictionary<string, string[]> MasculineTag = new Dictionary<string, string[]>();
}
