﻿using System;

namespace Assets.Scripts.TextGeneration
{
    static class BodyPartLinker
    {
        internal static BodyPartDescriptionType GetType(
            BodyPartType part,
            BodyPartDescription description
        )
        {
            return (BodyPartDescriptionType)
                Enum.Parse(typeof(BodyPartDescriptionType), $"{part}{description}");
        }
    }
}
