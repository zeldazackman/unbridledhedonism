﻿using System;
using System.Collections.Generic;
using System.Linq;

static class InteractionList
{
    static internal SortedDictionary<InteractionType, InteractionBase> List;

    static InteractionList()
    {
        var test = AppDomain.CurrentDomain
            .GetAssemblies()
            .SelectMany(x => x.GetTypes())
            .Where(x => typeof(InteractionBase).IsAssignableFrom(x) && !x.IsAbstract);
        List = new SortedDictionary<InteractionType, InteractionBase>();
        foreach (var obj in test)
        {
            InteractionBase instance = (InteractionBase)Activator.CreateInstance(obj);
            List[instance.Type] = instance;
        }

        //#warning temp code
        //        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //        foreach (var interaction in List)
        //        {
        //            var i = interaction.Value;
        //            sb.AppendLine($"{i.Name}#{i.Description}#{i.Class}#{i.Type}#{i.Range}#{i.SoundRange}#{i.Streaming}#{i.StreamingDescription}#{i.AsksPlayer}#{i.UsedOnPrey}#{i.Hostile} ");
        //        }
        //        System.IO.File.WriteAllText(UnityEngine.Application.dataPath + "\\testInteractions.csv", sb.ToString());
    }
}
