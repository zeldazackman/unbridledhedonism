﻿using OdinSerializer;

class Record
{
    [OdinSerialize]
    SimpleString Text;

    [OdinSerialize]
    readonly int ExpiresTurn;

    const int TurnsKept = 10;

    internal bool Expired()
    {
        return State.World.Turn >= ExpiresTurn;
    }

    public Record(Interaction interaction)
    {
        //var text = TextGenerator.GenerateMessage(interaction.Actor, interaction.Target, interaction.Success, interaction.Type, interaction.Stage);
        var text = MessageManager.GetText(interaction);
        if (text != "")
            Text = new SimpleString(interaction.Actor, interaction.Target, text);
        else
            Text = new SimpleString(
                interaction.Actor,
                interaction.Target,
                $"{interaction.Actor.FirstName} {interaction.Type} {interaction.Target.FirstName} {(interaction.Success ? "Success" : "Fail")}"
            );
        ExpiresTurn = State.World.Turn + TurnsKept;
    }

    public Record(SelfAction selfAction)
    {
        //var text = TextGenerator.GenerateSelfMessage(selfAction.Actor, selfAction.Type);
        var text = MessageManager.GetText(selfAction);
        if (text != "")
            Text = new SimpleString(selfAction.Actor, null, text);
        else
            Text = new SimpleString(
                selfAction.Actor,
                null,
                $"{selfAction.Actor.FirstName} {selfAction.Type}"
            );
        ExpiresTurn = State.World.Turn + TurnsKept;
    }

    public Record(SexInteraction sexAction)
    {
        //var text = TextGenerator.GenerateSexMessage(sexAction.Actor, sexAction.Target, sexAction.Type);
        var text = MessageManager.GetText(sexAction);
        if (text != "")
            Text = new SimpleString(sexAction.Actor, sexAction.Target, text);
        else
            Text = new SimpleString(
                sexAction.Actor,
                sexAction.Target,
                $"{sexAction.Actor.FirstName} {sexAction.Type} {sexAction.Target.FirstName}"
            );
        ExpiresTurn = State.World.Turn + TurnsKept;
    }

    public Record(SimpleString text)
    {
        Text = text;
        ExpiresTurn = State.World.Turn + TurnsKept;
    }

    internal bool InvolvesMe(Person person)
    {
        if (Text != null)
        {
            return Text.Actor == person || Text.Target == person;
        }
        return false;
    }

    internal bool AmActor(Person person)
    {
        if (Text != null)
        {
            return Text.Actor == person;
        }
        return false;
    }

    internal bool AmTarget(Person person)
    {
        if (Text != null)
        {
            return Text.Target == person;
        }
        return false;
    }

    internal bool GlobalEvent()
    {
        if (Text != null)
        {
            return Text.Actor == null;
        }
        return false;
    }

    public override string ToString()
    {
        if (Text != null)
        {
            return Text.Text;
        }
        return "";
    }
}

public class Interaction
{
    [OdinSerialize]
    public Person Actor;

    [OdinSerialize]
    public Person Target;

    [OdinSerialize]
    public bool Success;

    [OdinSerialize]
    public InteractionType Type;

    [OdinSerialize]
    public int Stage;

    public Interaction(
        Person actor,
        Person target,
        bool success,
        InteractionType type,
        int stage = 0
    )
    {
        Actor = actor;
        Target = target;
        Success = success;
        Type = type;
        Stage = stage;
    }
}

class SimpleString
{
    [OdinSerialize]
    internal Person Actor;

    [OdinSerialize]
    internal Person Target;

    [OdinSerialize]
    internal string Text;

    public SimpleString(Person actor, Person target, string text)
    {
        Actor = actor;
        Target = target;
        Text = text;
    }
}

public class SelfAction
{
    [OdinSerialize]
    public Person Actor;

    [OdinSerialize]
    public SelfActionType Type;

    public SelfAction(Person actor, SelfActionType type)
    {
        Actor = actor;
        Type = type;
    }
}

public class SexInteraction
{
    [OdinSerialize]
    public Person Actor;

    [OdinSerialize]
    public Person Target;

    [OdinSerialize]
    internal SexInteractionType Type;

    [OdinSerialize]
    public SexPosition Position;

    public SexInteraction(Person actor, Person target, SexInteractionType type)
    {
        Actor = actor;
        Target = target;
        Type = type;
        Position = actor.ActiveSex?.Position ?? SexPosition.Standing;
    }
}
