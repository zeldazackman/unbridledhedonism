﻿using OdinSerializer;

public enum SexPosition
{
    Standing,
    LyingDown,
    KneelingGiving,
    KneelingReceiving,
    SixtyNine,
    Missionary,
    BehindGiving,
    BehindReceiving
}

public class ActiveSex
{
    [OdinSerialize]
    internal Person Self;

    [OdinSerialize]
    internal Person Other;

    [OdinSerialize]
    internal SexPosition Position;

    [OdinSerialize]
    internal bool Receiving;

    [OdinSerialize]
    internal bool Willing;

    [OdinSerialize]
    internal int LastPositionChange;

    [OdinSerialize]
    internal int Turns;

    //public bool Involves (Person person)
    //{
    //    return person == Self || person == Other;
    //}

    public ActiveSex(Person self, Person other)
    {
        Self = self;
        Other = other;
        Position = SexPosition.Standing;
        Receiving = false;
        Willing = true;
        LastPositionChange = 2;
    }

    internal void EndSex()
    {
        Self.EndStreamingActions();
        Other.EndStreamingActions();
    }
}
