﻿class GPP
{
    internal static string HeIs(Person person, bool capitalize = false)
    {
        if (person.GenderType.PronounSet == PronounSet.She)
            if (capitalize)
                return "She is";
            else
                return "she is";
        if (person.GenderType.PronounSet == PronounSet.He)
            if (capitalize)
                return "He is";
            else
                return "he is";
        if (capitalize)
            return "They are";
        else
            return "they are";
    }

    internal static string HeHas(Person person, bool capitalize = false)
    {
        if (person.GenderType.PronounSet == PronounSet.She)
            if (capitalize)
                return "She has";
            else
                return "she has";
        if (person.GenderType.PronounSet == PronounSet.He)
            if (capitalize)
                return "He has";
            else
                return "he has";
        if (capitalize)
            return "They have";
        else
            return "they have";
    }

    internal static string He(Person person)
    {
        if (person.GenderType.PronounSet == PronounSet.She)
            return "she";
        if (person.GenderType.PronounSet == PronounSet.He)
            return "he";
        return "they";
    }

    internal static string SIfSingular(Person person)
    {
        if (person.GenderType.PronounSet == PronounSet.They)
            return "";
        return "s";
    }

    internal static string ESIfSingular(Person person)
    {
        if (person.GenderType.PronounSet == PronounSet.They)
            return "";
        return "es";
    }

    internal static string IESIfSingular(Person person)
    {
        if (person.GenderType.PronounSet == PronounSet.They)
            return "y";
        return "ies";
    }

    internal static string His(Person person)
    {
        if (person.GenderType.PronounSet == PronounSet.She)
            return "her";
        if (person.GenderType.PronounSet == PronounSet.He)
            return "his";
        return "their";
    }

    internal static string Him(Person person)
    {
        if (person.GenderType.PronounSet == PronounSet.She)
            return "her";
        if (person.GenderType.PronounSet == PronounSet.He)
            return "him";
        return "them";
    }

    internal static string Himself(Person person)
    {
        if (person.GenderType.PronounSet == PronounSet.She)
            return "herself";
        if (person.GenderType.PronounSet == PronounSet.He)
            return "himself";
        return "themselves";
    }

    internal static string Bastard(Person person) =>
        person.GenderType.PronounSet == PronounSet.She ? "bitch" : "bastard";
}
