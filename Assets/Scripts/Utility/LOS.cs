﻿using System;

static class LOS
{
    /// <summary>
    /// Alternate version that checks if the person is eaten, also uses that person for the start
    /// </summary>
    /// <param name="person"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public static bool Check(Person person, Vec2 end)
    {
        if (person.BeingEaten && person.Position != end)
            return false;
        return Check(person.Position, end);
    }

    public static bool Check(Vec2 start, Vec2 end)
    {
        if (start.GetNumberOfMovesDistance(end) > State.World.Settings.MaxSightRange)
            return false;
        int w = end.x - start.x;
        int h = end.y - start.y;
        int dx1 = 0,
            dy1 = 0,
            dx2 = 0,
            dy2 = 0;
        if (w < 0)
            dx1 = -1;
        else if (w > 0)
            dx1 = 1;
        if (h < 0)
            dy1 = -1;
        else if (h > 0)
            dy1 = 1;
        if (w < 0)
            dx2 = -1;
        else if (w > 0)
            dx2 = 1;
        int longest = Math.Abs(w);
        int shortest = Math.Abs(h);
        bool wide = true;
        if (!(longest > shortest))
        {
            longest = Math.Abs(h);
            shortest = Math.Abs(w);
            if (h < 0)
                dy2 = -1;
            else if (h > 0)
                dy2 = 1;
            dx2 = 0;
            wide = false;
        }
        int numerator = longest >> 1;
        for (int i = 0; i < longest; i++)
        {
            if (State.World.GetZone(start) == null)
                return false;
            numerator += shortest;
            if (!(numerator < longest))
            {
                Vec2 prevStart = start;
                numerator -= longest;
                //if (State.World.Settings.SeeThroughWallsEnabled == false && State.World.BorderController.GetBorder(start, new Vec2(prevStart.x, prevStart.y + dy1)).BlocksSight) Alternate extra-permissive
                if (wide)
                {
                    start.x += dx1;
                    if (start != prevStart)
                    {
                        if (
                            State.World.Settings.SeeThroughWallsEnabled == false
                            && State.World.BorderController.GetBorder(start, prevStart).BlocksSight
                        )
                            return false;
                        prevStart = start;
                    }
                    start.y += dy1;
                    if (start != prevStart)
                    {
                        if (
                            State.World.Settings.SeeThroughWallsEnabled == false
                            && State.World.BorderController.GetBorder(start, prevStart).BlocksSight
                        )
                            return false;
                    }
                }
                else
                {
                    start.y += dy1;
                    if (start != prevStart)
                    {
                        if (
                            State.World.Settings.SeeThroughWallsEnabled == false
                            && State.World.BorderController.GetBorder(start, prevStart).BlocksSight
                        )
                            return false;
                        prevStart = start;
                    }
                    start.x += dx1;
                    if (start != prevStart)
                    {
                        if (
                            State.World.Settings.SeeThroughWallsEnabled == false
                            && State.World.BorderController.GetBorder(start, prevStart).BlocksSight
                        )
                            return false;
                    }
                }
            }
            else
            {
                Vec2 prevStart = start;
                start.x += dx2;
                start.y += dy2;
                if (
                    State.World.Settings.SeeThroughWallsEnabled == false
                    && State.World.BorderController.GetBorder(start, prevStart).BlocksSight
                )
                    return false;
            }
        }
        return true;
    }
}
