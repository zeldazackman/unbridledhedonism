﻿using System;
using System.Collections;
using System.Globalization;
using UnityEngine;

static class MiscUtilities
{
    /// <summary>
    /// Invokes the specified action after the specified period of time
    /// </summary>
    public static void DelayedInvoke(Action theDelegate, float time)
    {
        State.GameManager.StartCoroutine(ExecuteAfterTime(theDelegate, time));
    }

    private static IEnumerator ExecuteAfterTime(Action action, float delay)
    {
        yield return new WaitForSeconds(delay);
        action();
    }

    public static string FancyHeight(float inches)
    {
        if (Config.UseMetric)
            return $"{Math.Round(inches * 2.54f, 1)} cm";
        return $"{Math.Floor(inches / 12)}' {Math.Round(inches % 12, 1)}\"";
    }

    public static string ConvertedHeight(float inches)
    {
        if (Config.UseMetric)
            return $"{Math.Round(inches * 2.54f, 1)} cm";
        return $"{Math.Round(inches % 12, 1)}\"";
    }

    public static string ConvertedWeight(float pounds)
    {
        if (Config.UseMetric)
            return $"{Math.Round(pounds * 0.453592f, 1)} kg";
        return $"{Math.Round(pounds, 1)} lbs";
    }
}

public static class StringExtensions
{
    public static string ToTitleCase(this string input)
    {
        return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input);
    }
}
