﻿using OdinSerializer;

static class Utility
{
    /// <summary>
    /// Takes the start value and moves it increase % of the way towards 1.
    /// For example 0.4 with an increase of .5 would become 0.7
    /// Note that if the ability score is negative, it can actually increase by more than the default max
    /// </summary>
    /// <param name="start"></param>
    /// <param name="increase"></param>
    /// <returns></returns>
    public static float PushTowardOne(float start, float increase) =>
        start + (increase * (1 - start));

    /// <summary>
    /// Takes the start value and moves it increase % of the way towards -1.
    /// For example 0.5 with an decrease of .5 would become -0.25
    /// Note that if the ability score is negative, it can actually decrease by more than the default max
    /// </summary>
    /// <param name="start"></param>
    /// <param name="decrease"></param>
    /// <returns></returns>
    public static float PushTowardsNegativeOne(float start, float decrease) =>
        start - (decrease * (1 + start));

    public static T1 SerialClone<T1>(T1 obj)
    {
        byte[] bytes = SerializationUtility.SerializeValue(obj, DataFormat.Binary);
        return SerializationUtility.DeserializeValue<T1>(bytes, DataFormat.Binary);
    }
}
