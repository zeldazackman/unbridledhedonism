﻿using UnityEngine;


public class BackgroundScript : MonoBehaviour
{

    [SerializeField] private float speed = 0.01f;

    private float offset;

    private Material material;

    void Start()
    {
        // material = GetComponent<SpriteRenderer>().material;
        // or
        material = GetComponent<Renderer>().material;
    }

    void Update()
    {
        offset += speed * Time.deltaTime;

        // with both methods, the result is the same
        // material.mainTextureOffset = new Vector2(offset, offset);
        material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}