Parent Race: Demi-Human
Selectable: True
Named Character: False

Hair Color, black, white, grey, orange, golden

Eye Color, black, white, red, orange, yellow, blue, green

Custom, Stripe Color, white, black, grey, red, orange, golden

Height, Feminine, 74, 88
Height, Masculine, 74, 88

Weight, .85, 1.10

Breast Size, Feminine, 4, 6

Dick Size, Masculine, 4, 6
Dick Size, Feminine, 4, 6
//Ball sizes are tied to the dick size, defaults to 85-115%
Ball Size, Masculine, 1.15, 1.30
Ball Size, Feminine, 1.15, 1.30