Parent Race: Reptile
Selectable: True
Named Character: False

Height, Feminine, 56, 72
Height, Masculine, 60, 76
Weight, 1.5, 2.5

Custom, Scale Pattern, striped, diamond, spotted, ringed
Custom, Pattern Color, blue, yellow, green, orange, cyan, red