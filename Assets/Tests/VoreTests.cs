﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class VoreTests
    {        
        [Test]
        public void VoreSystemTest()
        {
            // Use the Assert class to test conditions
            List<Person> people = new List<Person>();
            Settings settings = new Settings
            {
                VoreAnger = 0,
                VoreKnowledge = true,
                FriendshipMultiplier = 1,
                RomanticMultiplier = 1,
                DigestType = DigestType.Both
            };

            Person first = new Person("First", "", 1, Orientation.All, true, new Vec2(0, 0));
            Person second = new Person("Second", "", 1, Orientation.All, true, new Vec2(0, 0));
            Person third = new Person("Third", "", 1, Orientation.All, true, new Vec2(0, 0));
            Person fourth = new Person("Fourth", "", 1, Orientation.All, true, new Vec2(0, 0));

            people.Add(first);
            people.Add(second);
            people.Add(third);
            people.Add(fourth);

            State.World = new World(people, settings, null,  false);
            //State.GameManager?.TileManager.DrawWorld();
            //State.GameManager.ClickedPerson = State.World.ControlledPerson;
            //State.GameManager.DisplayInfo();
            //State.GameManager.CenterCameraOnTile(State.World.ControlledPerson.Position);

            foreach (Person i in people)
            {
                foreach (Person j in people)
                {
                    if (i == j)
                        continue;
                    SetHighRelation(i.GetRelationshipWith(j));
                }
            }

            first.Personality.PreyWillingness = .5f;
            second.Personality.PreyWillingness = .5f;
            third.Personality.PreyWillingness = .5f;
            fourth.Personality.PreyWillingness = .5f;

            first.Position = new Vec2(9, 9);
            second.Position = new Vec2(9, 9);
            third.Position = new Vec2(9, 9);
            fourth.Position = new Vec2(9, 9);
            second.AI.UseInteractionOnTarget(third, InteractionType.AskToOralVore);
            first.AI.UseInteractionOnTarget(second, InteractionType.AskToOralVore);

            first.Needs.ChangeEnergy(4);
            first.Personality.Voracity = 1;

            Assert.True(first.VoreController.GetProgressOf(second) != null);
            Assert.True(second.VoreController.GetProgressOf(third) != null);
            Assert.True(second.BeingEaten);
            Assert.True(third.BeingEaten);
            Assert.True(first.VoreController.GetProgressOf(second).Willing);
            Assert.True(second.VoreController.GetProgressOf(third).Willing);
            first.VoreController.GetProgressOf(second).Stage = 10;
            second.VoreController.GetProgressOf(third).Stage = 10;

            InteractionList.List[InteractionType.SpitUpPrey].OnSucceed(second, third);
            Assert.True(second.BeingEaten);
            Assert.True(third.BeingEaten);
            Assert.True(first.VoreController.GetProgressOf(second) != null);
            Assert.True(first.VoreController.GetProgressOf(third) != null);
            Assert.True(second.VoreController.GetProgressOf(third) == null);
            Assert.True(first.VoreController.GetProgressOf(second).Willing);
            Assert.True(first.VoreController.GetProgressOf(third).Willing);
            first.VoreController.GetProgressOf(third).Stage = 10;

            InteractionList.List[InteractionType.PreyEatOtherPrey].OnSucceed(second, third);
            //second.AI.UseInteractionOnTarget(third, InteractionType.PreyEatOtherPrey);
            Assert.True(first.VoreController.GetProgressOf(second) != null);
            Assert.True(second.VoreController.GetProgressOf(third) != null);
            Assert.True(second.BeingEaten);
            Assert.True(third.BeingEaten);
            Assert.True(first.VoreController.GetProgressOf(second).Willing);
            Assert.True(second.VoreController.GetProgressOf(third).Willing == false);

            second.VoreController.GetProgressOf(third).Stage = 10;

            InteractionList.List[InteractionType.SpitUpPrey].OnSucceed(second, third);
            Assert.True(second.BeingEaten);
            Assert.True(third.BeingEaten);
            Assert.True(first.VoreController.GetProgressOf(second) != null);
            Assert.True(first.VoreController.GetProgressOf(third) != null);
            Assert.True(second.VoreController.GetProgressOf(third) == null);
            Assert.True(first.VoreController.GetProgressOf(second).Willing);
            Assert.True(first.VoreController.GetProgressOf(third).Willing == false);

            InteractionList.List[InteractionType.SpitUpPrey].OnSucceed(first, second);
            InteractionList.List[InteractionType.SpitUpPrey].OnSucceed(first, third);
            Assert.True(second.BeingEaten == false);
            Assert.True(third.BeingEaten == false);
            Assert.True(first.VoreController.HasPrey(VoreLocation.Any) == false);



            //State.World.UpdatePlayerUI();

            //fourth.AI.UseInteractionOnTarget(first, InteractionType.Vore);

            void SetHighRelation(Relationship relation)
            {
                relation.Met = true;
                relation.FriendshipLevel = .99f;
                relation.RomanticLevel = .99f;
            }
        }

        //[Test]
        //public void TextTest()
        //{
        //    List<Person> people = new List<Person>();
        //    Settings settings = new Settings
        //    {
        //        VoreAcceptance = 1,
        //        VoreKnowledge = true,
        //        FriendshipMultiplier = 1,
        //        RomanticMultiplier = 1,
        //        DigestType = DigestType.Both
        //    };
        //    Person first = new Person("First", "", 1, Orientation.All, true, new Vec2(0, 0));


        //    people.Add(first);

        //    State.World = new World(people, settings, false);
        //    MessageManager msg = new MessageManager();
        //    msg.Interpret();
        //}

        //// A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        //// `yield return null;` to skip a frame.
        //[UnityTest]
        //public IEnumerator FirstTestsWithEnumeratorPasses()
        //{
        //    // Use the Assert class to test conditions.
        //    // Use yield to skip a frame.
        //    yield return null;
        //}
    }

    [TestFixture]
    public class InteractionTests
    {
        Person Pred;
        Person PreyA;
        Person PreyB;
        Person PreyOfPreyA;
        Person StandaloneA;
        Person SexA;
        Person SexB;

        [SetUp]
        public void Init()
        {
            List<Person> people = new List<Person>();
            Settings settings = new Settings
            {
                VoreAnger = 0,
                VoreKnowledge = true,
                FriendshipMultiplier = 1,
                RomanticMultiplier = 1,
                DigestType = DigestType.Both
            };

            Pred = new Person("Pred", "A", 0, Orientation.All, true, new Vec2(0, 0));
            PreyA = new Person("PreyA", "A", 0, Orientation.All, true, new Vec2(0, 0));
            PreyB = new Person("PreyB", "A", 0, Orientation.All, true, new Vec2(0, 0));
            PreyOfPreyA = new Person("PreyOfPreyA", "A", 0, Orientation.All, true, new Vec2(0, 0));
            StandaloneA = new Person("StandaloneA", "A", 0, Orientation.All, true, new Vec2(0, 0));
            SexA = new Person("SexA", "A", 0, Orientation.All, true, new Vec2(0, 0));
            SexB = new Person("SexB", "A", 0, Orientation.All, true, new Vec2(0, 0));
            InteractionList.List[InteractionType.AskToOralVore].OnSucceed(PreyA, PreyOfPreyA);
            PreyA.VoreController.GetProgressOf(PreyOfPreyA).Stage = 10;
            InteractionList.List[InteractionType.AskToOralVore].OnSucceed(Pred, PreyA);
            Pred.VoreController.GetProgressOf(PreyA).Stage = 10; 
            InteractionList.List[InteractionType.AskToOralVore].OnSucceed(Pred, PreyB);
            Pred.VoreController.GetProgressOf(PreyB).Stage = 10;
            InteractionList.List[InteractionType.StartSex].OnSucceed(SexA, SexB);
            people.Add(Pred);
            people.Add(PreyA);
            people.Add(PreyB);
            people.Add(PreyOfPreyA);
            people.Add(StandaloneA);
            people.Add(SexA);
            people.Add(SexB);
            State.World = new World(people, settings, null, false);
            State.World.TestingMode = true;
        }

        [TearDown]
        public void Cleanup()
        { /* ... */ }

        [Test]
        public void InteractionsThere()
        {
            bool missing = false;
            var sexInteractions = ((SexInteractionType[])Enum.GetValues(typeof(SexInteractionType))).ToList();            
            foreach (var action in sexInteractions)
            {
                if (SexInteractionList.List.ContainsKey(action) == false)
                {
                    Debug.LogWarning($"Missing interaction data for {action}");
                    missing = true;
                }
            }

            var selfActions = ((SelfActionType[])Enum.GetValues(typeof(SelfActionType))).ToList();
            selfActions.Remove(SelfActionType.None);
            foreach (var action in selfActions)
            {
                if (SelfActionList.List.ContainsKey(action) == false)
                {
                    Debug.LogWarning($"Missing interaction data for {action}");
                    missing = true;
                }
            }
            
            var interactions = ((InteractionType[])Enum.GetValues(typeof(InteractionType))).ToList();
            interactions.Remove(InteractionType.None);
            foreach (var action in interactions)
            {
                if ((int)action >= 1000)
                    continue;
                if (InteractionList.List.ContainsKey(action) == false)
                {
                    Debug.LogWarning($"Missing interaction data for {action}");
                    missing = true;
                }
            }
            Assert.IsFalse(missing);
        }

        [Test]
        public void PassTime()
        {
            for (int i = 0; i < 2500; i++)
            {
                State.World.NextTurnTesting(false);
            }
        }

        [Test]
        public void PassTime2()
        {
            State.World.Settings.AnalVoreEnabled = true;
            State.World.Settings.AnalVoreGoesDirectlyToStomach = true;
            State.World.Settings.DisposalEnabled = true;
            State.World.Settings.DisposalBonesEnabled = true;
            State.World.Settings.DisposalCockEnabled = true;
            for (int i = 0; i < 2500; i++)
            {
               
                State.World.NextTurnTesting(false);
            }
        }

        [Test]
        public void PassTime3()
        {
            State.World.Settings.AnalVoreEnabled = true;
            State.World.Settings.AnalVoreGoesDirectlyToStomach = false;
            State.World.Settings.NursesActive = true;
            for (int i = 0; i < 2500; i++)
            {
                State.World.NextTurnTesting(false);
            }
        }

        [Test]
        public void PassTime4()
        {
            State.World.Settings.WeightGain = true;
            State.World.Settings.WeightGainBody = 1;
            State.World.Settings.WeightGainBoob = 1;
            State.World.Settings.WeightGainDick = 1;
            State.World.Settings.WeightGainHeight = 1;
            for (int i = 0; i < 2500; i++)
            {
                State.World.NextTurnTesting(false);
            }
        }

        [Test]
        public void DoAllInteractions()
        {
            foreach (var type in (InteractionType[])Enum.GetValues(typeof(InteractionType)))
            {
                if (type.ToString().Contains("Vore"))
                    continue;
                foreach (var actor in State.World.GetPeople(true))
                {
                    foreach (var target in State.World.GetPeople(true))
                    {
                        if (target != actor)
                        {
                            if (InteractionList.List.ContainsKey(type))
                            {
                                var interaction = InteractionList.List[type];
                                if ((interaction.Class == ClassType.VoreConsuming || interaction.Class == ClassType.VoreAskThem || interaction.Class == ClassType.VoreAskToBe) && target.VoreController.ContainsPerson(actor, VoreLocation.Any))
                                    continue;                                
                                if (interaction.AppearConditional(actor, target))
                                {
                                    interaction.OnSucceed(actor, target);
                                }
                            }
                            
                        }
                    }
                }
            }
        }

    }
}
